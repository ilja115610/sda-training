**Changed logic of application **

* Person is created based on provided personal information (Factory method used)
* Added repositories of different vehicle types (findVehicle method is very primitive, just finds any vehicle matched)
* Vehicle brand added as enum
* Strategy of choosing a vehicle implemented (can add any other strategy based on Engine type, Fuel etc)
* Date Parser Utils enhanced