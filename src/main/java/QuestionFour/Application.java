package QuestionFour;

import QuestionFour.constants.VehicleBrand;
import QuestionFour.entity.Person;
import QuestionFour.entity.Vehicle;
import QuestionFour.personFactory.PersonFactory;
import QuestionFour.strategy.BasedOnBrandStrategy;

import java.time.LocalDate;
import java.util.Scanner;

public class Application {

    private static final Scanner in = new Scanner(System.in);


    public static void main(String[] args) {


        System.out.println("Enter personal data");
        PersonFactory factory = PersonFactory.getInstance();
        Person person = factory.createPerson(in.nextLine());

        System.out.println("Choose the vehicle you need to buy: \n 1. Car \n 2. Bike");
        int type = Integer.parseInt(in.nextLine());

        Vehicle chosenVehicle = person.chooseVehicle(new BasedOnBrandStrategy(),type);
        person.setVehicle(chosenVehicle);
        person.setVehiclePurchaseDate(LocalDate.now());

            System.out.println(person.toString());
            System.out.println("Bought "
                    + person.getVehicle().getType().name()+" ,"
                    +person.getVehicle().getBrand().name()
                    + " on " + person.getVehiclePurchaseDate());
        }
    }

