package QuestionFour.entity;

import QuestionFour.constants.VehicleBrand;
import QuestionFour.constants.VehicleType;

public class Bike extends Vehicle {
    public Bike(VehicleBrand brand) {
        this.type = VehicleType.BIKE;
        this.brand = brand;
    }
}
