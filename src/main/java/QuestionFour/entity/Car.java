package QuestionFour.entity;

import QuestionFour.constants.VehicleBrand;
import QuestionFour.constants.VehicleType;

public class Car extends Vehicle {
    public Car(VehicleBrand brand) {
        this.type = VehicleType.CAR;
        this.brand = brand;
    }
}
