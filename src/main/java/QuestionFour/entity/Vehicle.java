package QuestionFour.entity;

import QuestionFour.constants.VehicleBrand;
import QuestionFour.constants.VehicleType;

abstract public class Vehicle {
    protected VehicleType type;
    protected VehicleBrand brand;

    public VehicleType getType() {
        return type;
    }

    public void setType(VehicleType type) {
        this.type = type;
    }

    public VehicleBrand getBrand() {
        return brand;
    }

    public void setBrand(VehicleBrand brand) {
        this.brand = brand;
    }
}
