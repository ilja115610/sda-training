package QuestionFour.personFactory;

import QuestionFour.constants.Constants;
import QuestionFour.entity.Person;
import QuestionFour.exception.DatePatternMismatchException;
import QuestionFour.utils.DateParserUtils;

import java.time.LocalDate;

public class PersonFactory {

    private static PersonFactory instance;

    private Person person;

    private PersonFactory () {

    }

     public static PersonFactory getInstance() {
        if(instance == null){
            instance = new PersonFactory();
        }
        return instance;
     }

    public Person createPerson(String personalData) {


        String[] userInfoArray = personalData.split(Constants.SPACE);

        if (userInfoArray.length == 4) {
            LocalDate dateOfBirth = null;
            if (DateParserUtils.matches(userInfoArray[3])) {
                dateOfBirth = DateParserUtils.formatDateFromString(userInfoArray[3]);
            } else {
                throw new DatePatternMismatchException();
            }

            String firstName = userInfoArray[0];
            String lastName = userInfoArray[1];
            this.person = new Person(
                    firstName,
                    lastName,
                    dateOfBirth);

            System.out.println("Profile Created");
        }
        return person;
    }
}
