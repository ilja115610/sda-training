package QuestionFour.repository;

import QuestionFour.constants.VehicleBrand;
import QuestionFour.entity.Vehicle;

public interface BikeRepository {


    public Vehicle addVehicle(Vehicle bike);

    void initializeRepository();

    public Vehicle findVehicle(VehicleBrand brand);

}
