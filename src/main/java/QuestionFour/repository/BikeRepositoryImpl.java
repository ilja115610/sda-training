package QuestionFour.repository;

import QuestionFour.constants.VehicleBrand;
import QuestionFour.entity.Bike;
import QuestionFour.entity.Vehicle;

import java.util.ArrayList;
import java.util.List;

public class BikeRepositoryImpl implements BikeRepository{


    private static BikeRepositoryImpl instance;
    private static List<Vehicle> bikeList = new ArrayList<>();

    private BikeRepositoryImpl () {
        initializeRepository();
    }

    public static BikeRepositoryImpl getInstance() {
        if(instance ==null){
            instance = new BikeRepositoryImpl();
        }
        return instance;
    }



    @Override
    public Vehicle addVehicle(Vehicle vehicle) {
        bikeList.add(vehicle);
        return vehicle;
    }

    @Override
    public void initializeRepository() {
        Vehicle bike1 = new Bike(VehicleBrand.BMW);
        Vehicle bike2 = new Bike(VehicleBrand.MERCEDES);
        Vehicle bike3 = new Bike(VehicleBrand.AUDI);

        addVehicle(bike1);
        addVehicle(bike2);
        addVehicle(bike3);
    }

    @Override
    public Vehicle findVehicle(VehicleBrand brand) {
        return bikeList.stream().filter(c->c.getBrand().equals(brand)).findAny().orElse(null);
    }
}
