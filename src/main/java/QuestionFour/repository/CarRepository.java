package QuestionFour.repository;

import QuestionFour.constants.VehicleBrand;
import QuestionFour.entity.Vehicle;

public interface CarRepository {


        void initializeRepository();

        Vehicle addVehicle(Vehicle vehicle);

        Vehicle findVehicle(VehicleBrand brand);
    }

