package QuestionFour.repository;

import QuestionFour.constants.VehicleBrand;
import QuestionFour.entity.Car;
import QuestionFour.entity.Vehicle;

import java.util.ArrayList;
import java.util.List;

public class CarRepositoryImpl implements CarRepository{

    private static CarRepositoryImpl instance;
    private static List<Vehicle> carList = new ArrayList<>();

    private CarRepositoryImpl () {
        initializeRepository();
    }

    public static CarRepositoryImpl getInstance() {
        if(instance ==null){
            instance = new CarRepositoryImpl();
        }

        return instance;
    }


    @Override
    public void initializeRepository() {
        Vehicle car1 = new Car(VehicleBrand.BMW);
        Vehicle car2 = new Car(VehicleBrand.MERCEDES);
        Vehicle car3 = new Car(VehicleBrand.AUDI);

        carList.add(car1);
        carList.add(car2);
        carList.add(car3);
    }

    @Override
    public Vehicle addVehicle(Vehicle vehicle) {
        carList.add(vehicle);
        return vehicle;
    }

    @Override
    public Vehicle findVehicle(VehicleBrand brand) {

        return carList.stream().filter(c->c.getBrand().equals(brand)).findAny().orElse(null);

    }
}
