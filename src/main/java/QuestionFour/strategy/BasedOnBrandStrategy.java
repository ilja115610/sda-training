package QuestionFour.strategy;

import QuestionFour.constants.VehicleBrand;
import QuestionFour.constants.VehicleType;
import QuestionFour.entity.Vehicle;
import QuestionFour.repository.BikeRepository;
import QuestionFour.repository.BikeRepositoryImpl;
import QuestionFour.repository.CarRepository;
import QuestionFour.repository.CarRepositoryImpl;

import java.util.Scanner;

public class BasedOnBrandStrategy implements iChoice{

    private final CarRepository carRepository;
    private final BikeRepository bikeRepository;


    public BasedOnBrandStrategy() {
        carRepository = CarRepositoryImpl.getInstance();
        bikeRepository = BikeRepositoryImpl.getInstance();
    }

    @Override
    public Vehicle chooseVehicle(int typeChoice) {

        System.out.println("Enter vehicle brand");
        String brand = new Scanner(System.in).nextLine();

        VehicleType type = null;
        switch (typeChoice) {
            case 1:
               type = VehicleType.CAR;
                break;
            case 2:
               type = VehicleType.BIKE;
                break;
            default:
                System.out.println("Incorrect option");
        }

        assert type != null;
        if(type.equals(VehicleType.CAR)){
            return carRepository.findVehicle(VehicleBrand.valueOf(brand));
        }
        else {
            return bikeRepository.findVehicle(VehicleBrand.valueOf(brand));
        }

    }
}
