package QuestionFour.strategy;

import QuestionFour.constants.VehicleBrand;
import QuestionFour.entity.Vehicle;

public interface iChoice {

    Vehicle chooseVehicle(int vehicleType);
}
