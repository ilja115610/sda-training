package QuestionFour.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

public class DateParserUtils {
    private static final String DATE_REGEX_PATTERN = "^\\d{1,2}(\\/|-)\\d{1,2}(\\/|-)\\d{4}";
    private static Pattern DATE_PATTERN = Pattern.compile(DATE_REGEX_PATTERN);
    private static DateTimeFormatter dtf;


    public static boolean matches(String date) {

        if (DATE_PATTERN.matcher(date).matches()) {
            if (date.contains("/")) {
                dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            } else {
                dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            }
            return true;
        } else {
            System.out.println("Wrong format");
            return false;
        }

    }
        public static LocalDate formatDateFromString (String date){
            return LocalDate.parse(date, dtf);
        }

    }

